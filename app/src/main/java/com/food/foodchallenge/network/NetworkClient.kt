package com.food.foodchallenge.network

import com.food.foodchallenge.BuildConfig
import com.food.foodchallenge.network.response.model.DetailsMealServer
import com.food.foodchallenge.network.response.model.SearchMealServer
import com.google.gson.GsonBuilder
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import io.reactivex.rxjava3.core.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.regex.Matcher
import java.util.regex.Pattern


open class NetworkClient {
    private val retrofit: Retrofit
    private val apiService: ApiService

    init {
        val httpclient = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BODY)
            httpclient.addInterceptor(logging)
        }
        val client = httpclient.build()
        retrofit = Retrofit.Builder()
            .baseUrl("https://themealdb.com/api/json/v1/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .client(client)
            .build()
        apiService = retrofit.create(ApiService::class.java)
    }

    fun searchMeals(query: String): Single<List<SearchMealServer>> {
        return apiService.searchMeals(query)
            .map { it.meals ?: throw IllegalStateException("Can't find meals for <<$query>>") }
    }

    fun detailsMeal(id: Int): Single<DetailsMealServer> {
        return apiService.detailsMeal(id)
            .map {
                // Fixme current implementation can be redone with GsonAdapter (but that moment should be discussed)
                val meals = it.meals ?: throw IllegalStateException("Can't find detail meal for <<$id>>")

                //seems we need only one item (details should be one i suppose)
                // Todo discuss with project manager
                val data = meals.first().asJsonObject

                val idMeal = data.get("idMeal").asInt
                val meal = data.get("strMeal").asString
                val area = data.get("strArea").asString
                val category = data.get("strCategory").asString
                val mealThumb = data.get("strMealThumb").asString
                val instructions = data.get("strInstructions").asString
                // we getting ingredients and measures as fields
                // by regex we can understan how many fields there and than parse
                val ingredientMeasureList = mutableListOf<Pair<String, String>>().apply {
                    val size = getFieldListFromJsonStr(data.toString(), "strIngredient").size
                    for (k in 1 until size) {
                        val ingredient = data.get("strIngredient$k")
                        val measure = data.get("strMeasure$k")
                        if (!ingredient.isJsonNull
                            && !measure.isJsonNull
                            && ingredient.asString.isNotBlank()
                            && measure.asString.isNotBlank()
                        ) {
                            add(Pair(ingredient.asString, measure.asString))
                        }
                    }
                }

                DetailsMealServer(
                    idMeal = idMeal,
                    strMeal = meal,
                    strArea = area,
                    strCategory = category,
                    strInstructions = instructions,
                    strMealThumb = mealThumb,
                    ingredientMeasureList = ingredientMeasureList,
                )
            }
    }

    /* сan be improved and return list */
    private fun getFieldListFromJsonStr(jsonStr: String, fieldName: String): List<String> {
        val fieldValues = mutableListOf<String>()
        val matcher: Matcher = Pattern.compile("$fieldName\\d*").matcher(jsonStr)
        while (matcher.find()) {
            if (matcher.group().trim().isNotEmpty()) {
                fieldValues.add(matcher.group().trim())
            }
        }
        return fieldValues
    }
}