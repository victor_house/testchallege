package com.food.foodchallenge.ui.searchflow

data class OpenDetailsState(val searchId: Int)